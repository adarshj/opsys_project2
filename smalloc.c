//smalloc.c
//a safer, though simple, version of malloc
// awirth
// apr 11

#include <stdlib.h>
#include <stdio.h>
#include "smalloc.h"

static size_t bytes = 0;

void *
smalloc(size_t size)
{
	void *p = malloc(size);
	if(p == NULL){
		fprintf(stderr,"There was not enough space on the heap. "
		"Aborting.\n");
		exit(EXIT_FAILURE);
	}
	bytes += size;
	return p;
}

void
bytes_used(void)
{
	printf("bytes used: %16lu\n",bytes);
}
