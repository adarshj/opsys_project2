all : proxy

proxy: main.o proxy.o smalloc.o logger.o
	$(CC) $(CFLAGS) -o proxy main.o proxy.o smalloc.o logger.o 

CC = gcc
CFLAGS = -lpthread -lsocket -lnsl 

main.o: Makefile proxy.h logger.h smalloc.h 

proxy.o: Makefile proxy.h smalloc.h

logger.o: Makefile logger.h
