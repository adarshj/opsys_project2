#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <synch.h>
#include "logger.h"

#define BUFFSIZE 256

//globals
char log_path[BUFFSIZE];
char *timep;
time_t lt;

//mutex
mutex_t mutex;

// function that initiates the log file by removing the old one, if it
// exists, and then creating a new one. 
void init_log(char *log_file_name)
{
    remove(log_file_name);
    log_path[0] = '\0';
    strcat(log_path, log_file_name);
    mutex_init(&mutex, NULL, NULL);
}

// function that writes to the log
void write_log(char *clientIP, int portno, int bytes_sent, char *host)
{        
    FILE *fp;
    int date, year;
    char *month = malloc(sizeof(char)*80);
    char *day = malloc(sizeof(char)*80);
    char *time_string = malloc(sizeof(char)*80);
    
    // get the date and time values
    get_date_time(&date, month, &year, day, time_string);
    
    mutex_lock(&mutex);
    //Start of critical section...
    if((fp = fopen(log_path, "a")) != NULL) {
         fprintf(fp, " %s %s %d %s %d,%s,%d,%d,%s\n",day, month, date, 
                        time_string, year, clientIP, portno, bytes_sent, 
                            host);
    } else {
         if((fp = fopen(log_path, "w")) != NULL) {
            fprintf(fp, " %s %s %d %s %d,%s,%d,%d,%s\n",day, month, date, 
                               time_string, year, clientIP, portno, 
                               bytes_sent,host);
         } else {
              perror("logging: cannot write to file");
              exit(1);
         }
    }
    fclose(fp);
    //end of critical section
    mutex_unlock(&mutex);
}

// get the date and time values
void get_date_time(int *date, char *month, int *year, char *day,
                    char *time_string)
{

    time(&lt);
    timep = asctime(localtime(&lt));
    char buffer[20];
    struct tm *sys_T = NULL;
    time_t tval = 0;
    tval = time(NULL);
    sys_T = localtime(&tval);
    //date
    *date = sys_T->tm_mday;
    //month
    strftime(buffer,20,"%B",sys_T);
    strcpy(month, buffer);
    //year
    *year = 1900 + sys_T->tm_year;
    //day of the week
    strftime(buffer,20,"%A",sys_T);
    strcpy(day, buffer);
    //time
    strftime(buffer,20,"%X",sys_T);
    strcpy(time_string, buffer);
}
