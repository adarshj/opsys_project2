#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h> 
#define MAX 150000
int main(int argc, char**argv)
{
  int sockfd, n;
  fd_set readfds;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  struct timeval tv;
  char *message = malloc(sizeof(char)*1000);
  char *url = "/";
  char buffer[150000];
  int portno = 80;
 /* Translate host name into peer's IP address;
  * This is name translation service by the 
  * operating system 
  */
  server = gethostbyname(argv[1]);
  if (server == NULL) 
  {
    fprintf(stderr,"ERROR, no such host\n");
    exit(0);
  }
  /* Building data structures for socket */
  bzero((char *) &serv_addr, sizeof(serv_adr));
  serv_addr.sin_family = AF_INET; 
  bcopy((char *)server->h_addr,  (char *)&serv_addr.sin_addr.s_addr,
  server->h_length);
  serv_addr.sin_port = htons(portno);
  /* Create TCP socket -- active open 
  * Preliminary steps: Setup: creation of 
  * active open socket
  */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) 
  {
    perror("ERROR opening socket");
    exit(0);
  }
  // try connecting to the server's socket 
  if (connect(sockfd, (struct sockaddr *)&serv_addr,
  sizeof(serv_addr)) < 0) 
  {
    perror("ERROR connecting");
    exit(0);
  }
  // the GET request that shall be used to 
  // retrieve content
  strcpy(message, "GET "); 
  strcat(message, url);
  strcat(message, " HTTP/1.0\r\n"); 
  strcat(message,"\r\n");
  // send this message to the web server
  send(sockfd, message,strlen(message), 0);
  bzero(buffer, MAX);
  FD_ZERO(&readfds);
  //add the descriptor to the set
  FD_SET(sockfd,&readfds);
  // the number of seconds that the client 
  // has to wait for
  tv.tv_sec = 3;
  tv.tv_usec = 0;
  n = sockfd + 1;
  int i = 0;
  int rv = select(n,&readfds,NULL,NULL,&tv);
  if (rv == -1)
  {
    perror("select");
  } else if (rv == 0)
  {
    // Timeout after 3s
    printf("%s\n", "Timeout!");
  }
  else 
  {
    // read and write the content
    while (recv(sockfd, buffer, MAX, 0) > 0) 
    {
      printf("%s\n", buffer);
    }
  }   
  close(sockfd);
  return 0;
}
