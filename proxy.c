#include <arpa/inet.h>
#include "proxy.h"
#include "logger.h"
#include "smalloc.h"

// globals
char *ext_path;
char clienip[INET_ADDRSTRLEN];
int clientport_no;
char host[MAX];

// function to get the relevant parts of the message
// sent from the client
void strget(char *buffer)
{
	int i = 0;
	ext_path = smalloc(sizeof(char)*1000);
	char *delims = "//";
	char *temphost = smalloc(sizeof(char)*1000);
	char *token = strtok(buffer,delims);
	while (token != NULL){
		// remove the intial parts of the message 
		if (i == 0){
			token = strtok(NULL,delims);
		}
		// get the host name from the message
		// assume there is a '/' after the url
    else if (i == 1) {
			temphost = token;
			token = strtok(NULL,delims);
			strcpy(ext_path,"/");
		}
		// add any URL paths other than the root
    // include accept headers
		else {
			strcat(ext_path,token);
			token = strtok(NULL,delims);
			// do not add a '/' after a URL 
      // otherwise add a '/'
			if ((token != NULL) && (strcmp(token," HTTP") != 0)){
				strcat(ext_path, "/");
			}	
		}
		i++;
	}
	for (i = 0; i < strlen(temphost) ; i++){
		host[i] = temphost[i];
	}	
}

// function to set up the server connection
int serv_setup()
{
	struct addrinfo hints,*res;
	res = (struct addrinfo *)smalloc(sizeof(struct addrinfo));
	int servsockfd;
	// the port number of the web server to connect to
	// assume always 80
	char *portno = "80";
  //get the server address
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;	
	getaddrinfo(host,portno,&hints,&res);
	servsockfd = socket(res->ai_family, res->ai_socktype, 
  res->ai_protocol);
	// connect to the web server
	if (connect(servsockfd,res->ai_addr,res->ai_addrlen) < 0) 
	{
		perror("ERROR connecting");
		exit(0);
	}
	return servsockfd;
}

// This function is the start of the proxy protocol
void proxy_protocol(int cliensockfd)
{
	int servsockfd;
	char response[MAX];
	char buffer[MAX];
	char *newmessage = smalloc(sizeof(char)*1000);
	int bytes_sent = 0;
	int i;
	int size = 0;
	bzero(host,strlen(host));
	bzero(buffer,MAX);
  //receive the message from the client
	recv(cliensockfd, buffer, MAX, 0);
  //get the important parts of the message
	strget(buffer);
  //set up the server connection
	servsockfd = serv_setup();
	strcpy(newmessage, "GET ");
	strcat(newmessage, ext_path);
	// the GET request that shall be used to retrieve content
	// send this message to the web server
	send(servsockfd, newmessage,strlen(newmessage), 0);
	bzero(buffer,MAX);
  // recieve and write to the client
  // can handle accept headers
	while (recv(servsockfd,buffer,MAX,0) > 0);
  // the size of the non string characters and the string characters
	size = wcslen(buffer) + strlen(buffer);
	bytes_sent += send(cliensockfd,buffer,size,0);
	// write to the proxy log
  write_log(clienip,clientport_no,bytes_sent,host); 
	close(servsockfd);
	close(cliensockfd);
}

// connect client to proxy
int client_connect_proxy(int sockfd)
{
	struct sockaddr_in cli_addr;
	int cliensockfd, clilen;
	/* Listen on socket - means we're ready to accept connections - 
	 incoming connection requests will be queued */
	listen(sockfd,5);
	clilen = sizeof(cli_addr);
	/* Accept a connection - block until a connection is ready to
	 be accepted. Get back a new file descriptor to communicate on. */
	cliensockfd = accept(sockfd, (struct sockaddr *) 
  &cli_addr,&clilen);
	if (cliensockfd < 0) 
	{
		perror("ERROR on accept");
		exit(1);
	}
	inet_ntop(AF_INET, &(cli_addr), clienip, INET_ADDRSTRLEN);
	//get the client port number
  clientport_no = cli_addr.sin_port;
	return cliensockfd;
}	


//fuction to set up the proxy
int proxy_setup(int portno)
{
	int cliensockfd, sockfd;
	struct sockaddr_in proxy_addr, cli_addr;
	int n;
	struct hostent *server;
	bzero((char *) &proxy_addr, sizeof(proxy_addr));
	
	/* Create address we're going to listen on (given port number)
	 - converted to network byte order & any IP address for 
	 this machine */
	
	proxy_addr.sin_family = AF_INET;
	proxy_addr.sin_addr.s_addr = INADDR_ANY;
  // store in machine-neutral format
	proxy_addr.sin_port = htons(portno);  
	
	/* Create TCP socket */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
	{
		perror("ERROR opening socket");
		exit(1);
	}
	
	/* Bind address to the socket */
	if (bind(sockfd, (struct sockaddr *) &proxy_addr,
			sizeof(proxy_addr)) < 0) 
	{
		perror("ERROR on binding");
		exit(1);
	}
	return sockfd;
	
}
