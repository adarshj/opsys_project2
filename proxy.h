#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#define TRUE (0==0)
#define FALSE (0==1)
#define MAX 150000

typedef int bool;

void strget(char *buffer);

int serv_setup();

int proxy_setup(int portno);

int client_connect_proxy(int sockfd);
	
void proxy_protocol(int cliensockfd);

