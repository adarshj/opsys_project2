#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "proxy.h"

int main(int argc, char **argv)
{
  pthread_t lth; //listener thread
	int portno = atoi(argv[1]);		
  // set up the proxy log file
  init_log("proxy.log");
  // set up the proxy
	int sd = proxy_setup(portno);
	int cd;
	int n;
	while(1)
  {
    //establish a client to the proxy
	  cd = client_connect_proxy(sd);
    //start the listener thread
	  pthread_create(&lth, NULL, (void *)&proxy_protocol,(void *) cd);
	}
	return 0; 
}


